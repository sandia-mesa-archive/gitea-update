#!/usr/bin/env bash
# Gitea Updater Script
# Programmer: Sean King
# Copyright: 2020 Sandia Mesa Animation Studios and other contributors
# License: WTFPL
#
# I'm sick of having to remember a whole bunch of steps in order to ship an update.
# I'm over-exaggerating, but I thought this process could be a whole lot easier to do
# quickly with a simple bash script. So, I thought I'd make this.
#
# All you do is git clone this project to the Gitea user’s home directory and set the
# script permssions properly. After that, go to the project folder under the Gitea user's
# home directory and run sudo ./gitea-update.sh. And voila! You're good to go!

# First, let's do a backup in case some BS happens.
# Source that I learned about runuser from: https://stackoverflow.com/questions/17956151/how-to-run-a-command-as-a-specific-user-in-an-init-script#31616592
echo "Creating backup..."; runuser git -s /bin/bash -c "gitea dump -c /etc/gitea/app.ini" > /dev/null

# Next, let's get the latest version tag from GitHub
# Learned how to do this for GitHub from here: https://blog.markvincze.com/download-artifacts-from-a-latest-github-release-in-sh-and-powershell/
echo "Getting information about latest version of Gitea from GitHub..."
LATEST_RELEASE=$(curl -L -s -H 'Accept: application/json' https://github.com/go-gitea/gitea/releases/latest)
LATEST_VERSION=$(echo $LATEST_RELEASE | sed -e 's/.*"tag_name":"v\([^"]*\)".*/\1/')

# Now, we'll get the latest version of Gitea from dl.gitea.io.
echo "Getting latest version of Gitea..."; wget -O gitea https://dl.gitea.io/gitea/${LATEST_VERSION}/gitea-${LATEST_VERSION}-linux-amd64 -q --show-progress

# Next, we'll move the new binary from Gitea.
echo "Moving new Gitea binary..."; mv gitea /usr/local/bin/gitea
chmod +x /usr/local/bin/gitea

# Finally, we'll restart Gitea.
echo "Restarted Gitea."; systemctl restart gitea
