# Gitea Updater Script

I'm sick of having to remember a whole bunch of steps in order to ship an update. I'm over-exaggerating, but I thought this process could be a whole lot easier to do quickly with a simple bash script. So, I thought I'd make this.

All you do is git clone this project to the Gitea user’s home directory and set the script permssions properly like seen below:

```
# Download the Gitea Updater script to the Gitea user's home directory. Note that this command can also be used to get the latest version of the script.
sudo -Hu git git clone https://code.sandiamesa.com/sandiamesa/gitea-update /home/git/gitea-update

# Set the Gitea Updater script to be executable.
sudo -Hu git chmod u+x /home/git/gitea-update/gitea-update.sh
```

After that, go to the project folder under the Gitea user's home directory and run sudo ./gitea-update.sh. And voila! You're good to go!

**Note:** This scripts assume you're running a Linux AMD-64 system (like Debian, Ubuntu, etc), installed it from binary, and that your Gitea user is git. If none of this describes your Gitea installation, you should probably consider forking/modifying this script.
